# coding=utf-8
__copyright__ = 'Copyright 2015, Abalt'

from oscar.apps.partner import config


class PartnerConfig(config.PartnerConfig):
    name = 'eurotaxes.partner'
