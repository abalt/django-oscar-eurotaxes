# coding=utf-8
__copyright__ = 'Copyright 2015, Abalt'

from oscar.apps.dashboard import config


class DashboardConfig(config.DashboardConfig):
    name = 'eurotaxes.dashboard'
