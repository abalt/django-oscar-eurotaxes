# coding=utf-8
__copyright__ = 'Copyright 2015, Abalt'

from oscar.apps.catalogue import config


class CatalogueConfig(config.CatalogueConfig):
    name = 'eurotaxes.catalogue'
